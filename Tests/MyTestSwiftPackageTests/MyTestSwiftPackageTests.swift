    import XCTest
    @testable import MyTestSwiftPackage

    final class MyTestSwiftPackageTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(MyTestSwiftPackage().text, "Hello, World!")
        }
    }
